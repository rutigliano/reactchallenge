# Mighty Mind React Challenge

This code challenge will test your:

- Ability to think on the spot
- Communication skills and ability to talk through their code with others
- Logical thought processes
  This coding challenge designed to be completed in 48 hours. We are going to evaluate the work that has been acheaved in the first 60 minutes of work but we also encourage the developer to work longer and try to complete the task. The task does not need to be completed for a successfull interview, but we will appreciate the effort in trying to complete the task. 

## Writing your code

Please look at the layout png image included in the repo. Think how to structure the HTML structure and how to split the elements in components and how to style them to look as the same as the picture.

You are encouraged to use the Material Library for styling the React components. It offers React components for faster and easier web development. You can find more at:

[Material UI Github](https://github.com/mui-org/material-ui)

[Material UI Official Website](https://material-ui.com/)

When Writing your code, please be mindful of the following:

-use eslint for js lint
-use stylelint for scss lint
-use editorconfig for editor configuration
-use packages.json to expose your scripts

The code must be submitted to public repo in gitlab and a live version has to be deployed on gitlab pages

Please add the public link to the README.md file on your codebase. The layout must be responsive and optimise for iPad and iPhone.

You are encouraged to use Material UI Components and implement custom styles using emotion

[emotion for react ](https://emotion.sh/docs/introduction)

[emotion and Material UI](https://networksynapse.net/development/mui-v5-material-with-emotion/)

## Installation

Create a new react application named material-ui run the code below.

```bash
npx create-react-app MightyMinds

yarn add @material-ui/core fontsource-roboto @material-ui/icons

yarn add @material-ui/core

yarn add @material-ui/icons

yarn add @emotion/react

yarn add @emotion/styled

yarn add --dev @emotion/babel-plugin

```

## Usage

In Index.js

```JavaScript
// After you installed all the required modules you can start using react with material-ui and style our components.
/** @jsxImportSource @emotion/react */
import React from "react";
import ReactDOM from "react-dom";
import { css } from "@emotion/react"
import Button from "@material-ui/core/Button";

const App = () => {
  return (
    <Button
    css={css`
             margin-left: 10px;
        `}
        variant="contained" color="primary">
      Mighty Minds
    </Button>
  );
}
```

# The Layout

![Portal Home](https://gitlab.com/rutigliano/reactchallenge/-/raw/main/PORTAL%20HOME.png)
